#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate eui48;

use rocket::Data;
use wake::{send_wol, CustomError};
use std::fs;
use std::io::Read;

#[post("/wake/<mac>", format="plain", data="<secret>")]
fn index(mac: String, secret: Data) -> Result<&'static str, CustomError> {
    let secret_actual = fs::read_to_string("secret").unwrap_or("".to_string());
    let mut secret_provided = String::new();
    if let Err(_) = secret.open().read_to_string(&mut secret_provided) {
        println!("Could not parse secret");
        return Err(CustomError::Parse("Could not parse secret"));
    }
    if secret_provided != secret_actual {
        println!("Wrong secret provided");
        return Err(CustomError::Unauthorized("You need to provide the correct secret"))
    }
    let result = send_wol(&mac[..]);
    match result {
        Ok(_) => {
            println!("Sent wol to {:?}", &mac);
            Ok("Sent wol package")
        },
        Err(error) => Err(error),
    }

}

fn main() {
    rocket::ignite().mount("/", routes![index]).launch();
}
