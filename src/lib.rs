extern crate wol;
#[macro_use] extern crate rocket;

use std::net::UdpSocket;
use std::io::Error;
use eui48::MacAddress;


#[derive(Responder,Debug)]
pub enum CustomError {
    #[response(status = 401)]
    Unauthorized(&'static str),
    #[response(status = 400)]
    Parse(&'static str),
    #[response(status = 400)]
    Send(&'static str),
}

pub fn send_wol(mac: &str) -> Result<(), CustomError> {
    let mac = MacAddress::parse_str(mac);
    match mac {
        Ok(mac) => {
            match send(mac) {
                Ok(_) => Ok(()),
                Err(_) => Err(CustomError::Send("Could not send wol package"))

            }
        },
        Err(_) => Err(CustomError::Parse("Could not parse mac address"))
    }
}

fn send(mac: MacAddress) -> Result<(), Error> {
    let broadcast_address = "255.255.255.255:9";
    let bind_address = "0.0.0.0:0";
    let mut packet = vec![0u8; 102];

    // The header is 6 0xFFs
    for i in 0..6 {
        packet[i] = 0xFF;
    }

    // We copy the mac address 16 times.
    for i in 0..16 {
        for j in 0..6 {
            packet[6 + (i * 6) + j] = mac.as_bytes()[j];
        }
    }

    let socket = UdpSocket::bind(bind_address)?;
    socket.set_broadcast(true)?;
    socket.send_to(&packet, broadcast_address)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

}
